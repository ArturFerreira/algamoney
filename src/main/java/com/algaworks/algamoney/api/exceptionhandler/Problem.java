package com.algaworks.algamoney.api.exceptionhandler;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Problem {
    private String mensagemUsuario;
    private String mensagemDesenvolvedor;
    private String detail;
    private Integer status;
    private LocalDateTime timestamp;
    private String type;
    private String title;
    private List<Object> objects;

    @Getter
    @Builder
    public static class Object {
        private String name;
        private String userMessage;
    }
}
