package com.algaworks.algamoney.api.controller;

import com.algaworks.algamoney.domain.model.Lancamento;
import com.algaworks.algamoney.domain.model.Pessoa;
import com.algaworks.algamoney.domain.projection.ResumoLancamento;
import com.algaworks.algamoney.domain.service.LancamentoService;
import com.algaworks.algamoney.repository.filter.LancamentoFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/lancamentos")
public class LancamentoController {

    @Autowired
    private LancamentoService lancamentoService;

    @GetMapping("/todos")
    public List<Lancamento> listar(){
        return lancamentoService.buscar();
    }

    @GetMapping
//    @PreAuthorize("hasAuthority('ROLE_PESQUISAR_LANCAMENTO') and hasAuthority('SCOPE_read')")
    public Page<Lancamento> pesquisar(LancamentoFilter lancamentoFilter, Pageable pageable){
        return lancamentoService.pesquisar(lancamentoFilter, pageable);
    }

    @GetMapping(params = "resumo")
//    @PreAuthorize("hasAuthority('ROLE_PESQUISAR_LANCAMENTO') and hasAuthority('SCOPE_read')")
    public Page<ResumoLancamento> pesquisarResumo(LancamentoFilter lancamentoFilter, Pageable pageable){
        return lancamentoService.pesquisarResumo(lancamentoFilter, pageable);
    }

    @GetMapping("/{codigo}")
//    @PreAuthorize("hasAuthority('ROLE_PESQUISAR_LANCAMENTO') and hasAuthority('SCOPE_read')")
    public Lancamento buscarPeloCodigo(@PathVariable Long codigo){
        return lancamentoService.buscarPeloCodigo(codigo);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
//    @PreAuthorize("hasAuthority('ROLE_CADASTRAR_LANCAMENTO') and hasAuthority('SCOPE_write')")
    public Lancamento salvar(@RequestBody @Valid Lancamento lancamento){
        return lancamentoService.salvar(lancamento);
    }

    @PutMapping("/{codigo}")
    @PreAuthorize("hasAuthority('ROLE_CADASTRAR_LANCAMENTO')")
    public Lancamento atualizar(@PathVariable Long codigo, @Valid @RequestBody Lancamento lancamento){
        return lancamentoService.atualizar(codigo, lancamento);
    }

    @DeleteMapping("/{codigo}")
//    @PreAuthorize("hasAuthority('ROLE_REMOVER_LANCAMENTO') and hasAuthority('SCOPE_write')")
    public void remover(@PathVariable Long codigo){
        lancamentoService.remover(codigo);
    }

}
