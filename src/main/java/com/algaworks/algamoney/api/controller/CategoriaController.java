package com.algaworks.algamoney.api.controller;

import com.algaworks.algamoney.domain.model.Categoria;
import com.algaworks.algamoney.domain.service.CategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/categorias")
public class CategoriaController {

    @Autowired
    private CategoriaService categoriaService;

    @GetMapping
//    @PreAuthorize("hasAuthority('ROLE_PESQUISAR_CATEGORIA') and hasAuthority('SCOPE_read')" )
    public List<Categoria> listar(){
        return categoriaService.buscar();
    }

    @GetMapping("/{codigo}")
//    @PreAuthorize("hasAuthority('ROLE_PESQUISAR_CATEGORIA') and hasAuthority('SCOPE_read')")
    public Categoria buscarPeloCodigo(@PathVariable Long codigo){
        return categoriaService.buscarPeloCodigo(codigo);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
//    @PreAuthorize("hasAuthority('ROLE_CADASTRAR_CATEGORIA')")
    public Categoria salvar(@RequestBody @Valid Categoria categoria){
        return categoriaService.salvar(categoria);
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
//    @PreAuthorize("hasAuthority('ROLE_CADASTRAR_CATEGORIA') and hasAuthority('SCOPE_write')")
    public void remover(@PathVariable Long codigo){
        categoriaService.remover(codigo);
    }
}
