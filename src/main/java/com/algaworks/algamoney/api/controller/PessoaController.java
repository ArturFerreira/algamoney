package com.algaworks.algamoney.api.controller;

import com.algaworks.algamoney.domain.model.Pessoa;
import com.algaworks.algamoney.domain.service.PessoaService;
import com.algaworks.algamoney.repository.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/pessoas")
public class PessoaController {

    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private PessoaService pessoaService;

//    @GetMapping
//    @PreAuthorize("hasAuthority('ROLE_PESQUISAR_PESSOA') and hasAuthority('SCOPE_read')")
//    public List<Pessoa> listar(){
//        return pessoaRepository.findAll();
//    }

    @GetMapping("/{codigo}")
//    @PreAuthorize("hasAuthority('ROLE_PESQUISAR_PESSOA') and hasAuthority('SCOPE_read')")
    public Optional<Pessoa> buscarPeloCodigo(@PathVariable Long codigo){
        return pessoaRepository.findById(codigo);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
//    @PreAuthorize("hasAuthority('ROLE_CADASTRAR_PESSOA') and hasAuthority('SCOPE_write')")
    public Pessoa adicionar(@RequestBody @Valid Pessoa pessoa){
        return pessoaRepository.save(pessoa);
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
//    @PreAuthorize("hasAuthority('ROLE_REMOVER_PESSOA') and hasAuthority('SCOPE_write')")
    public void remover(@PathVariable Long codigo){
        pessoaService.remover(codigo);
    }

    @PutMapping("/{codigo}")
    @PreAuthorize("hasAuthority('ROLE_CADASTRAR_PESSOA') and hasAuthority('SCOPE_write')")
    public Pessoa atualizar(@PathVariable Long codigo, @Valid @RequestBody Pessoa pessoa){
        return pessoaService.atualizar(codigo, pessoa);
    }

    @PutMapping("/{codigo}/ativo")
    @ResponseStatus(HttpStatus.NO_CONTENT)
//    @PreAuthorize("hasAuthority('ROLE_CADASTRAR_PESSOA') and hasAuthority('SCOPE_write')")
    public Pessoa atualizarParcialPropreidadeAtivo(@PathVariable Long codigo, @RequestBody @Valid Boolean ativo){
        return pessoaService.atualizarParcialPropreidadeAtivo(codigo, ativo);
    }

    @GetMapping
//    @PreAuthorize("hasAuthority('ROLE_PESQUISAR_PESSOA') and hasAuthority('SCOPE_read')")
    public Page<Pessoa> pesquisar(@RequestParam(required = false, defaultValue = "") String nome, Pageable pageable) {
        return pessoaRepository.findByNomeContaining(nome, pageable);
    }

}
