package com.algaworks.algamoney.repository;

import com.algaworks.algamoney.domain.model.Lancamento;
import com.algaworks.algamoney.domain.projection.ResumoLancamento;
import com.algaworks.algamoney.repository.filter.LancamentoFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface LancamentoRepositoryQueries {
    Page<Lancamento> filtrarLancamento(LancamentoFilter lancamentoFilter, Pageable pageable);
    Page<ResumoLancamento> resumirLancamento(LancamentoFilter lancamentoFilter, Pageable pageable);
}