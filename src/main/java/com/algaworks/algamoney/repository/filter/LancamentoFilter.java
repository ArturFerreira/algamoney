package com.algaworks.algamoney.repository.filter;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.Calendar;

@Data
public class LancamentoFilter {
    private String descricao;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Calendar dataVencimentoDe;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Calendar dataVencimentoAte;

}
