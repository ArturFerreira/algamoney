package com.algaworks.algamoney.domain.projection;

import com.algaworks.algamoney.domain.model.TipoLancamento;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Calendar;

@Data
public class ResumoLancamento {
    private Long codigo;
    private String descricao;
    private Calendar dataVencimento;
    private Calendar dataPagamento;
    private BigDecimal valor;
    private TipoLancamento tipo;
    private String categoria;
    private String pessoa;

    public ResumoLancamento(){
    }

    public ResumoLancamento(Long codigo, String descricao, Calendar dataVencimento, Calendar dataPagamento,
                            BigDecimal valor, TipoLancamento tipo, String categoria, String pessoa) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.dataVencimento = dataVencimento;
        this.dataPagamento = dataPagamento;
        this.valor = valor;
        this.tipo = tipo;
        this.categoria = categoria;
        this.pessoa = pessoa;
    }
}
