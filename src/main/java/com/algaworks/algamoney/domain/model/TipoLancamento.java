package com.algaworks.algamoney.domain.model;

public enum TipoLancamento {
    RECEITA,
    DESPESA
}
