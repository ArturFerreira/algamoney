package com.algaworks.algamoney.domain.exception;

public class LancamentoNaoEncontradoException extends EntidadeNaoEncontradaException{
    public static final long serialVersionUID = 1L;

    public LancamentoNaoEncontradoException(String message){
        super(message);
    }

    public LancamentoNaoEncontradoException(Long lancamentoId){
        this(String.format("Não existe um cadastro de lancamento com código %d", lancamentoId));
    }
}
