package com.algaworks.algamoney.domain.exception;

public class CategoriaNaoEncontradaException extends EntidadeNaoEncontradaException{
    public static final long serialVersionUID = 1L;

    public CategoriaNaoEncontradaException(String message){
        super(message);
    }

    public CategoriaNaoEncontradaException(Long categoriaId){
        this(String.format("Não existe um cadastro de lancamento com código %d", categoriaId));
    }
}
