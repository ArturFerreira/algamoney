package com.algaworks.algamoney.domain.service;

import com.algaworks.algamoney.domain.exception.LancamentoNaoEncontradoException;
import com.algaworks.algamoney.domain.exception.PessoaNaoEncontradaException;
import com.algaworks.algamoney.domain.model.Categoria;
import com.algaworks.algamoney.domain.model.Lancamento;
import com.algaworks.algamoney.domain.model.Pessoa;
import com.algaworks.algamoney.domain.projection.ResumoLancamento;
import com.algaworks.algamoney.repository.LancamentoRepository;

import com.algaworks.algamoney.repository.filter.LancamentoFilter;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class LancamentoService {

    @Autowired
    private LancamentoRepository lancamentoRepository;

    @Autowired
    private CategoriaService categoriaService;

    @Autowired
    private PessoaService pessoaService;

    public List<Lancamento> buscar() {
        return lancamentoRepository.findAll();
    }

    public Page<Lancamento> pesquisar(LancamentoFilter lancamentoFilter, Pageable pageable) {
        return lancamentoRepository.filtrarLancamento(lancamentoFilter, pageable);
    }

    public Page<ResumoLancamento> pesquisarResumo(LancamentoFilter lancamentoFilter, Pageable pageable) {
        return lancamentoRepository.resumirLancamento(lancamentoFilter, pageable);
    }

    @Transactional
    public void remover(Long codigo){
        try {
            lancamentoRepository.deleteById(codigo);
        } catch (EmptyResultDataAccessException ex) {
            throw new LancamentoNaoEncontradoException(codigo);
        }
    }

    public Lancamento buscarPeloCodigo(Long lancamentoId) {
        return lancamentoRepository.findById(lancamentoId)
                .orElseThrow(() -> new LancamentoNaoEncontradoException(lancamentoId));
    }

    @Transactional
    public Lancamento atualizar(Long codigo, Lancamento lancamento){
        Lancamento lancamentoSalva = buscarPeloCodigo(codigo);

        Long categoriaId = lancamento.getCategoria().getCodigo();
        Long pessoaId = lancamento.getPessoa().getCodigo();
        Categoria categoria = categoriaService.buscarPeloCodigo(categoriaId);
        Pessoa pessoa = pessoaService.buscarPeloCodigo(pessoaId);

        if (!pessoa.getAtivo())
            throw new PessoaNaoEncontradaException(pessoaId);

        lancamentoSalva.setCategoria(categoria);
        lancamentoSalva.setPessoa(pessoa);

        BeanUtils.copyProperties(lancamento, lancamentoSalva, "codigo");
        return lancamentoRepository.save(lancamentoSalva);
    }

    @Transactional
    public Lancamento salvar(Lancamento lancamento){
        Long categoriaId = lancamento.getCategoria().getCodigo();
        Long pessoaId = lancamento.getPessoa().getCodigo();
        Categoria categoria = categoriaService.buscarPeloCodigo(categoriaId);
        Pessoa pessoa = pessoaService.buscarPeloCodigo(pessoaId);

        if (!pessoa.getAtivo())
            throw new PessoaNaoEncontradaException(pessoaId);

        lancamento.setCategoria(categoria);
        lancamento.setPessoa(pessoa);

        return lancamentoRepository.save(lancamento);
    }

}
