package com.algaworks.algamoney.domain.service;

import com.algaworks.algamoney.domain.exception.EntidadeEmUsoException;
import com.algaworks.algamoney.domain.exception.PessoaNaoEncontradaException;
import com.algaworks.algamoney.domain.model.Pessoa;
import com.algaworks.algamoney.repository.PessoaRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PessoaService {

    @Autowired
    private PessoaRepository pessoaRepository;

    private static final String MSG_PESSOA_EM_USO
            = "Pessoa de código %d não pode ser removida, pois está em uso";

    @Transactional
    public void remover(Long codigo){
        try {
            pessoaRepository.deleteById(codigo);
            pessoaRepository.flush();
        } catch (EmptyResultDataAccessException ex) {
            throw new PessoaNaoEncontradaException(codigo);
        } catch (DataIntegrityViolationException ex){
            throw new EntidadeEmUsoException(String.format(MSG_PESSOA_EM_USO, codigo));
        }
    }

    public Pessoa buscarPeloCodigo(Long pessoaId) {
        return pessoaRepository.findById(pessoaId)
                .orElseThrow(() -> new PessoaNaoEncontradaException(pessoaId));
    }

    @Transactional
    public Pessoa atualizar(Long codigo, Pessoa pessoa){
        Pessoa pessoaSalva = buscarPeloCodigo(codigo);
        BeanUtils.copyProperties(pessoa, pessoaSalva, "codigo");
        return pessoaRepository.save(pessoaSalva);
    }

    @Transactional
    public Pessoa atualizarParcialPropreidadeAtivo(Long codigo, Boolean ativo){
        Pessoa pessoaSalva = buscarPeloCodigo(codigo);
        pessoaSalva.setAtivo(ativo);
        return pessoaRepository.save(pessoaSalva);
    }

}
