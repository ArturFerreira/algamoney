package com.algaworks.algamoney.domain.service;

import com.algaworks.algamoney.domain.exception.CategoriaNaoEncontradaException;
import com.algaworks.algamoney.domain.exception.EntidadeEmUsoException;
import com.algaworks.algamoney.domain.model.Categoria;
import com.algaworks.algamoney.repository.CategoriaRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CategoriaService {

    @Autowired
    private CategoriaRepository categoriaRepository;

    private static final String MSG_CATEGORIA_EM_USO
            = "Categoria de código %d não pode ser removida, pois está em uso";

    @Transactional
    public void remover(Long codigo){
        try {
            categoriaRepository.deleteById(codigo);
            categoriaRepository.flush();
        } catch (EmptyResultDataAccessException ex) {
            throw new CategoriaNaoEncontradaException(codigo);
        } catch (DataIntegrityViolationException ex){
            throw new EntidadeEmUsoException(String.format(MSG_CATEGORIA_EM_USO, codigo));
        }
    }

    public Categoria buscarPeloCodigo(Long categoriaId) {
        return categoriaRepository.findById(categoriaId)
                .orElseThrow(() -> new CategoriaNaoEncontradaException(categoriaId));
    }

    public List<Categoria> buscar() {
        return categoriaRepository.findAll();
    }

    @Transactional
    public Categoria atualizar(Long codigo, Categoria categoria){
        Categoria categoriaSalva = buscarPeloCodigo(codigo);
        BeanUtils.copyProperties(categoria, categoriaSalva, "codigo");
        return categoriaRepository.save(categoriaSalva);
    }

    @Transactional
    public Categoria salvar(Categoria categoria){
        return categoriaRepository.save(categoria);
    }

}
