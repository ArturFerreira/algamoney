CREATE table lancamento(
codigo bigint identity(1,1),
descricao varchar(50) not null,
data_vencimento datetime not null,
data_pagamento datetime,
valor numeric(16,2) not null,
observacao varchar(100),
tipo varchar(30) not null,
codigo_categoria bigint not null,
codigo_pessoa bigint not null,
primary key (codigo),

constraint fk_lancamento_categoria foreign key (codigo_categoria) references categoria (codigo),
constraint fk_lancamento_pessoa foreign key (codigo_pessoa) references pessoa (codigo),

)
