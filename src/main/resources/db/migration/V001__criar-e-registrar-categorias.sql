CREATE table categoria(
  codigo bigint identity(1,1),
  nome varchar(50) not null,
  primary key (codigo),
)

insert into categoria VALUES ('Lazer');
insert into categoria VALUES ('Alimentação');
insert into categoria VALUES ('Supermercado');
insert into categoria VALUES ('Farmácia');
insert into categoria VALUES ('Outros');
