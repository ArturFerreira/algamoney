create table pessoa(
    codigo bigint identity(1,1),
    nome varchar(20),
    logradouro varchar(50),
    numero varchar(5),
    complemento varchar(50),
    bairro varchar(20),
    cep varchar(20),
    cidade varchar(20),
    estado varchar(20),
    primary key (codigo),
)