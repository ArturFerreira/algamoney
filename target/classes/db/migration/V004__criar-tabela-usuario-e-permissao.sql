CREATE TABLE usuario (
codigo BIGINT identity(1,1),
nome VARCHAR(50) NOT NULL,
email VARCHAR(50) NOT NULL,
senha VARCHAR(150) NOT NULL,
primary key (codigo),
)

CREATE TABLE permissao (
codigo BIGINT identity(1,1),
descricao VARCHAR(50) NOT NULL,
primary key (codigo)
)

CREATE TABLE usuario_permissao (
codigo_usuario BIGINT NOT NULL,
codigo_permissao BIGINT NOT NULL,

primary key (codigo_usuario, codigo_permissao),

constraint fk_usuario_permissao_usuario foreign key (codigo_usuario) references usuario (codigo),
constraint fk_usuario_permissao_permissao foreign key (codigo_permissao) references permissao (codigo)
)

INSERT INTO usuario ( nome, email, senha) values ( 'Administrador', 'admin@algamoney.com', '$2a$10$X607ZPhQ4EgGNaYKt3n4SONjIv9zc.VMWdEuhCuba7oLAL5IvcL5.');
INSERT INTO usuario ( nome, email, senha) values ( 'Maria Silva', 'maria@algamoney.com', '$2a$10$Zc3w6HyuPOPXamaMhh.PQOXvDnEsadztbfi6/RyZWJDzimE8WQjaq');

INSERT INTO permissao ( descricao) values ( 'ROLE_CADASTRAR_CATEGORIA');
INSERT INTO permissao ( descricao) values ( 'ROLE_PESQUISAR_CATEGORIA');

INSERT INTO permissao ( descricao) values ( 'ROLE_CADASTRAR_PESSOA');
INSERT INTO permissao ( descricao) values ( 'ROLE_REMOVER_PESSOA');
INSERT INTO permissao ( descricao) values ( 'ROLE_PESQUISAR_PESSOA');

INSERT INTO permissao ( descricao) values ( 'ROLE_CADASTRAR_LANCAMENTO');
INSERT INTO permissao ( descricao) values ( 'ROLE_REMOVER_LANCAMENTO');
INSERT INTO permissao ( descricao) values ( 'ROLE_PESQUISAR_LANCAMENTO');

-- admin
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 1);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 2);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 3);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 4);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 5);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 6);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 7);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 8);

-- maria
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (2, 2);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (2, 5);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (2, 8);